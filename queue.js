let collection = [];

// Write the queue functions below.

// Print
function print() {
    return collection
}


// Enqueue
function enqueue(x){
	collection[collection.length] = x
	return collection
}


// Dequeue
function dequeue() {
	collection.shift()
	return collection
}


// Front
function front() {
    return collection[0]
}


// Size
function size() {
    return collection.length
}


// isEmpty
function isEmpty() {
    return collection.length ? false : true
}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
